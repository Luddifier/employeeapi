package se.scania.luddifier.employeedataversion2.models.mappers;

import org.apache.tomcat.jni.Local;
import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeesHiredWithin2yearsDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

 public class EmployeesHiredLast2YearsMapper {


    public static EmployeesHiredWithin2yearsDTO Employees2yearsMap(Map<Integer, Employee> employeeMap) {
        HashMap<String, String> recentHire = new HashMap<>();
        for (Map.Entry<Integer, Employee> entry : employeeMap.entrySet()) {
            var dateHired = LocalDate.parse(entry.getValue().getDateHired());
            var twoYearsAgo = LocalDate.now().minusYears(2);

            if (dateHired.isAfter(twoYearsAgo)) {

                recentHire.put("Name: " + entry.getValue().getFirstName() + " " + entry.getValue().getLastName(), "Unique Code " + entry.getValue().getUniqueCode());
            }


        }

        return new EmployeesHiredWithin2yearsDTO((Map<String, String>) recentHire);

        }
}
