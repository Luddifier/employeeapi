package se.scania.luddifier.employeedataversion2.models.dto;

import java.util.List;
import java.util.Map;

public class EmployeeHeldDevicesDTO {

    private Map<String, Long> employeeDevicesCount;

    public EmployeeHeldDevicesDTO(Map<String, Long> employeeDevicesCount) {
        this.employeeDevicesCount = employeeDevicesCount;
    }

    public Map<String, Long> getEmployeeDevicesCount() {
        return employeeDevicesCount;
    }

    public void setEmployeeDevicesCount(Map<String, Long> employeeDevicesCount) {
        this.employeeDevicesCount = employeeDevicesCount;
    }
}



