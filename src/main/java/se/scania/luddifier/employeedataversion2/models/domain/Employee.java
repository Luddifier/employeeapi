package se.scania.luddifier.employeedataversion2.models.domain;

import java.util.List;

public class Employee {

    private int id;
    private Long uniqueCode;
    private String firstName;
    private String lastName;
    private String dob;
    private String employeeTimerType;
    private String position;
    private String dateHired;
    private List<String> devicesUsed;

    public Employee() {}

    public Employee(int id, String firstName, String lastName)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Employee(int id, Long uniqueCode, String firstName, String lastName, String dob, String employeeTimerType, String position, String dateHired, List<String> devicesUsed) {
        this.id = id;
        this.uniqueCode = uniqueCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.employeeTimerType = employeeTimerType;
        this.position = position;
        this.dateHired = dateHired;
        this.devicesUsed = devicesUsed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(Long uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmployeeTimerType() {
        return employeeTimerType;
    }

    public void setEmployeeTimerType(String employeeTimerType) {
        this.employeeTimerType = employeeTimerType;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDateHired() {
        return dateHired;
    }

    public void setDateHired(String dateHired) {
        this.dateHired = dateHired;
    }

    public List<String> getDevicesUsed() {
        return devicesUsed;
    }

    public void setDevicesUsed(List<String> devicesUsed) {
        this.devicesUsed = devicesUsed;
    }
}
