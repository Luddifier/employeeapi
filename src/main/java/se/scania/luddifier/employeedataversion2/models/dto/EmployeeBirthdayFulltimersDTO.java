package se.scania.luddifier.employeedataversion2.models.dto;

import java.util.ArrayList;
import java.util.Map;

public class EmployeeBirthdayFulltimersDTO {


        private Map<String, ArrayList<String>> fullTimersBirthdays;

        public EmployeeBirthdayFulltimersDTO(Map<String, ArrayList<String>> birthDays) {
            this.fullTimersBirthdays= birthDays;
        }

        public Map<String, ArrayList<String>> getFullTimersBirthdays() {
            return fullTimersBirthdays;
        }

        public void setFullTimersBirthdays(Map<String, ArrayList<String>> birthday ) {
            this.fullTimersBirthdays = fullTimersBirthdays;
        }
    }

