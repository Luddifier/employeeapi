package se.scania.luddifier.employeedataversion2.models.mappers;

import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeHeldDevicesDTO;

import java.util.*;

public class devicesHeldMapper {

    public static EmployeeHeldDevicesDTO employeeDevicesMap(Map<Integer, Employee> employeeMap) {
        Map<String, Long> employeeDevices = new HashMap<>();
        Iterator<Integer> iterator = employeeMap.keySet().iterator();

        while (iterator.hasNext()) {
            int key = iterator.next();
            // For the specific key, get first name and lastname, then create full name
            String firstName = employeeMap.get(key).getFirstName();
            String lastName = employeeMap.get(key).getLastName();
            String fullName = firstName + " " + lastName;

            // get devices and put them in a new arrayList, then count them.
            List<String> devices = employeeMap.get(key).getDevicesUsed();
            Long numberOfDevices = (long) devices.size();
            employeeDevices.put(fullName, numberOfDevices);
        }
        return new EmployeeHeldDevicesDTO(employeeDevices);
    }
}



