package se.scania.luddifier.employeedataversion2.models.dto;

import java.util.Map;

public class EmployeesHiredWithin2yearsDTO {

    private Map<String, String> employeesHiredLast2Years;

    public EmployeesHiredWithin2yearsDTO(Map<String, String> employeesLast2Years) {
        this.employeesHiredLast2Years = employeesLast2Years;
    }

    public Map<String, String> getEmployeesHiredLast2Years() {
        return employeesHiredLast2Years;
    }

    public void setEmployeesHiredLast2Years(Map<String, String> employeesHiredLast2Years) {
        this.employeesHiredLast2Years = employeesHiredLast2Years;
    }




}
