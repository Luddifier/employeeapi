package se.scania.luddifier.employeedataversion2.models.mappers;

import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeBirthdayFulltimersDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static java.time.temporal.ChronoUnit.DAYS;

public class FullTimerBirthDaysMapper {

    public static EmployeeBirthdayFulltimersDTO mapBirthdays(Map<Integer, Employee> employeeMap) {
        Map<String, ArrayList<String>> employeeBirthdays = new HashMap<>();

        Iterator<Integer> iterator = employeeMap.keySet().iterator();

        //While there is a next key
        while (iterator.hasNext()) {
            int key = iterator.next();
            // Only care about fulltimers
            if (employeeMap.get(key).getEmployeeTimerType().equals("Full-time")) {
                // Concatenate full name
                String firstName = employeeMap.get(key).getFirstName();
                String lastName = employeeMap.get(key).getLastName();
                String fullName = firstName + " " + lastName;

                // Get the date of birth
                ArrayList<String> birthdayList = new ArrayList<>();
                String dateOfBirth = String.valueOf(employeeMap.get(key).getDob());
                birthdayList.add(dateOfBirth);

                // Calculate the number of days until the next birthday and print it out
                String daysUntilBirthday = daysUntilBirthday(employeeMap.get(key).getDob());
                birthdayList.add("Days until next birthday: " + daysUntilBirthday);

                employeeBirthdays.put(fullName, birthdayList);

            }
        }

        return new EmployeeBirthdayFulltimersDTO(employeeBirthdays);
    }

        private static String daysUntilBirthday(String inputStringDate){
            // daysUntilBirthday calculates the number of days between today and next birthday.

            LocalDate employeeDob = LocalDate.parse(inputStringDate);

            LocalDate today = LocalDate.now();

            LocalDate nextBirthday = employeeDob.withYear(today.getYear());

            // if days between is negative, add one year to next birthday.
            if (DAYS.between(today, nextBirthday) < 0) {
                nextBirthday = nextBirthday.plusYears(1);
            }
            return String.valueOf(DAYS.between(today, nextBirthday));


        }

}