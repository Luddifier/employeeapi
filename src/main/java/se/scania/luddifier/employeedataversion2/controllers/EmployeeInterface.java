package se.scania.luddifier.employeedataversion2.controllers;

import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeBirthdayFulltimersDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeHeldDevicesDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeesHiredWithin2yearsDTO;

import java.util.HashMap;

public interface EmployeeInterface {

    HashMap<Integer, Employee> getAllEmployees();
    Employee createEmployee(int id, Employee employee);
    Employee replaceEmployee(int id, Employee employee);
    Employee getEmployee(int id);

    Employee deleteEmployee(int id);

    Employee modifyEmployee(int id, Employee employee);

    EmployeeHeldDevicesDTO getHeldDevices();

    EmployeesHiredWithin2yearsDTO getRecentHires();

    EmployeeBirthdayFulltimersDTO getEmployeesBirthdays();
}