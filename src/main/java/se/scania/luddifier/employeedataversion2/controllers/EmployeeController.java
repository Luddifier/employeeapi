package se.scania.luddifier.employeedataversion2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.scania.luddifier.employeedataversion2.dataaccess.EmployeeRepository;
import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeBirthdayFulltimersDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeHeldDevicesDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeesHiredWithin2yearsDTO;

import java.util.HashMap;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeRepository repository;

    @Autowired
    public EmployeeController(EmployeeRepository repository) {
        this.repository = repository;
    }

    @GetMapping("")
    public ResponseEntity<HashMap> getAllEmployees() {
        return new ResponseEntity<>(repository.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{value}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int value) {
        if (!repository.employeeExistsById(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(repository.getEmployee(value), HttpStatus.OK);

    }

    @PostMapping("/create")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        //Luhn validation is in the 'isACtualEmployee' function
        if (!repository.isActualEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        int value = employee.getId();

        return new ResponseEntity<>(repository.createEmployee(value, employee), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> replaceEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!repository.isActualEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(repository.replaceEmployee(id, employee), HttpStatus.OK);
    }

    @PutMapping("/delete/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable int id) {
        if (!repository.employeeExistsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(repository.deleteEmployee(id), HttpStatus.OK);
    }



    @PatchMapping("/{id}")
    public ResponseEntity<Employee> modifyEmployee(@PathVariable int id, @RequestBody Employee employee) {

        if (!EmployeeRepository.employeeExistsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(repository.modifyEmployee(id, employee), HttpStatus.OK);
    }

    @GetMapping("/devices/summary")
    public ResponseEntity<EmployeeHeldDevicesDTO> getDevicesSummary() {
        return new ResponseEntity<>(repository.getHeldDevices(), HttpStatus.OK);
    }

    @GetMapping("/new")
    public ResponseEntity<EmployeesHiredWithin2yearsDTO> getNewEmployees() {
        return new ResponseEntity<>(repository.getRecentHires(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<EmployeeBirthdayFulltimersDTO> getFulltimeEmployeesBirthdays() {
        // Should be able to display it better
        return new ResponseEntity<>(repository.getEmployeesBirthdays(), HttpStatus.OK);
    }


}