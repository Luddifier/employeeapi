package se.scania.luddifier.employeedataversion2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeDataVersion2Application {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeDataVersion2Application.class, args);
    }

}
