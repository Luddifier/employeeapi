package se.scania.luddifier.employeedataversion2.dataaccess;

import org.springframework.stereotype.Repository;
import se.scania.luddifier.employeedataversion2.controllers.EmployeeInterface;
import se.scania.luddifier.employeedataversion2.models.domain.Employee;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeBirthdayFulltimersDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeeHeldDevicesDTO;
import se.scania.luddifier.employeedataversion2.models.dto.EmployeesHiredWithin2yearsDTO;
import se.scania.luddifier.employeedataversion2.models.mappers.EmployeesHiredLast2YearsMapper;
import se.scania.luddifier.employeedataversion2.models.mappers.FullTimerBirthDaysMapper;
import se.scania.luddifier.employeedataversion2.models.mappers.devicesHeldMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeRepository implements EmployeeInterface {

    //creates the basic data structure with 3 employees in it
    public static Map<Integer, Employee> employeeMap;
    {
        employeeMap = new HashMap<>();
        employeeMap.put(1, new Employee(1, 6876878189L,"R2",    "D2",         "1977-12-16", "Full-time", "Omniscientist", "2020-01-22", List.of("built in interface", "taser gun", "sensor array")));
        employeeMap.put(2, new Employee(2, 3364417513L, "Ludvig", "Bowallius", "1984-04-17", "Full-time", "Software Developer", "2021-04-13", List.of("PC", "Creativity Interface", "Virtual Reality DNC")));
        employeeMap.put(3, new Employee(3, 2575020462L, "Thomas", "Andersson", "1964-09-02", "Half-time", "Senior Developer", "2019-03-30", List.of("Neural interface", "Skill circuitry")));
    }

    //Below are the basic parts of the CRUD
    @Override
    public HashMap<Integer, Employee> getAllEmployees() {
        return (HashMap<Integer, Employee>) employeeMap;
    }

    @Override
    public Employee getEmployee(int id) {
        return employeeMap.get(id);
    }

    //See isActualEmployee function for luhn algorithm usage
    @Override
    public Employee createEmployee(int id, Employee employee) {
        employeeMap.put(id, employee);
        return getEmployee(employee.getId());
    }

    @Override
    public Employee replaceEmployee(int id, Employee employee) {
        employeeMap.replace(id, employee);
        return getEmployee(employee.getId());

    }

    @Override
    public Employee deleteEmployee(int id) {
        employeeMap.remove(id);
        return employeeMap.get(id);
    }

    @Override
    public Employee modifyEmployee(int id, Employee employee) {
        var employeeToModify = employeeMap.get(id);
         //WHAT IS REASONABLE TO ADD HERE BELOW?
        if (employee.getFirstName() != null) {employeeToModify.setFirstName(employee.getFirstName());}
        if (employee.getFirstName() != null) {employeeToModify.setLastName(employee.getLastName());}
        if (employee.getDob() != null) {employeeToModify.setDob(employee.getDob());}
        if (employee.getEmployeeTimerType() != null) {employeeToModify.setEmployeeTimerType(employee.getEmployeeTimerType());}
        if (employee.getPosition() != null) {employeeToModify.setPosition(employee.getPosition());}
        if (employee.getDateHired() != null) {employeeToModify.setDateHired(employee.getDateHired());}
        if (employeeToModify.getDevicesUsed() == null) {employeeToModify.setDevicesUsed(employee.getDevicesUsed());}
        if (employeeToModify.getDevicesUsed() != null) {employeeToModify.getDevicesUsed().addAll(employee.getDevicesUsed());}

        return employeeToModify;
    }

    public static boolean employeeExistsById(int id) {
        return employeeMap.containsKey(id);
    }

    public static boolean isActualEmployee(Employee employee) {
        //note reference to LuhnAlgorithmValidator
        return employee.getId() > 0 && LuhnValidator.validateEmployeeByCode(employee.getUniqueCode()) && employee.getFirstName() != null && employee.getLastName() != null && employee.getDob() != null && employee.getEmployeeTimerType() != null && employee.getPosition() != null && employee.getDateHired() != null && employee.getDevicesUsed() != null;
    }

    public static boolean employeeExists(Employee employee) {
        return employeeMap.containsKey(employee.getId());
    }

    @Override
    public EmployeeHeldDevicesDTO getHeldDevices() {
        return devicesHeldMapper.employeeDevicesMap(employeeMap);
    }

    @Override
    public EmployeesHiredWithin2yearsDTO getRecentHires() {
        return EmployeesHiredLast2YearsMapper.Employees2yearsMap(employeeMap);
    }

    @Override
    public EmployeeBirthdayFulltimersDTO getEmployeesBirthdays() {
        return FullTimerBirthDaysMapper.mapBirthdays(employeeMap);
    }



    //public boolean isValidGuitar(Guitars guitar, int id) {
    //    return isValidGuitar(guitar) && guitar.getId() == id;}

}
