package se.scania.luddifier.employeedataversion2.dataaccess;

import java.util.Arrays;

public class LuhnValidator {

    public static boolean validateEmployeeByCode(Long inputCode) {
        String stringifiedInputCode = String.valueOf(inputCode);
        if (stringifiedInputCode.length() != 10 || !stringifiedInputCode.matches("[0-9]*$")) {
            return false;
        }

        // int array for processing the employee code
        int[] employeeCodeArray = new int[stringifiedInputCode.length()];

        // Checks and adds the even entries according to the rules of the luhn algorithm
        for (int i = 0; i < stringifiedInputCode.length(); i++) {
            char c = stringifiedInputCode.charAt(i);
            employeeCodeArray[i] = Integer.parseInt("" + c);
        }

        // Checks and adds the odd odd entries according to the rules of the luhn algorithm
        for (int i = employeeCodeArray.length - 2; i >= 0; i = i - 2) {
            int num = employeeCodeArray[i];
            num = num * 2;
            if (num > 9) {
                num = num % 10 + num / 10;
            }
            employeeCodeArray[i] = num;
        }

        // final summation and check below
        int sum = Arrays.stream(employeeCodeArray).sum();

        return sum % 10 == 0;
    }
}
