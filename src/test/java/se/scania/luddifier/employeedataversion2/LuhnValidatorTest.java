package se.scania.luddifier.employeedataversion2;

import org.junit.jupiter.api.Test;
import se.scania.luddifier.employeedataversion2.dataaccess.EmployeeRepository;
import se.scania.luddifier.employeedataversion2.dataaccess.LuhnValidator;
import se.scania.luddifier.employeedataversion2.models.domain.Employee;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LuhnValidatorTest {

    @Test
    void IsValidUniqueCode() {
        //A correctly performed test where all parameters should be correctly fed in
        Employee guineaPig = new Employee(6, 2250456015L, "red", "shirt", "1966-09-08", "Full-timer", "sacrificial lamb", "2020-11-13", List.of("red shirt", "phaser"));
        boolean expectedResult = true;

        boolean resultedResult = EmployeeRepository.isActualEmployee(guineaPig);
        assertEquals(expectedResult, resultedResult);
    }

    @Test
    void isTooShortUniqueCode() {
        //Will return incorrect result wtih only 7 numbers in the unique code
        Employee guineaPig = new Employee(6, 2250456L, "red", "shirt", "1966-09-08", "Full-timer", "sacrificial lamb", "2020-11-13", List.of("red shirt", "phaser"));
        boolean expectedResult = false;

        boolean resultedResult = EmployeeRepository.isActualEmployee(guineaPig);
        assertEquals(expectedResult, resultedResult);

    }

    @Test
    void isNotAValidLuhnNumber() {
        //Will return false since the number provided is not a valid Luhn number

        Employee guineaPig = new Employee(6, 5238298921L, "red", "shirt", "1966-09-08", "Full-timer", "sacrificial lamb", "2020-11-13", List.of("red shirt", "phaser"));
        boolean expectedResult = false;

        boolean resultedResult = EmployeeRepository.isActualEmployee(guineaPig);
        assertEquals(expectedResult, resultedResult);
    }


}