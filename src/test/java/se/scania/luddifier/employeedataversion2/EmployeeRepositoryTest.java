package se.scania.luddifier.employeedataversion2;

import org.junit.jupiter.api.Test;
import se.scania.luddifier.employeedataversion2.dataaccess.EmployeeRepository;
import se.scania.luddifier.employeedataversion2.models.domain.Employee;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeRepositoryTest {

    //Instantiates the repository
    EmployeeRepository repository = new EmployeeRepository();

    @Test
    void getAllEmployees_initialCollectionExists_shouldReturnThreeEmployees() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int totalExpectedEmployees = 3;
        int totalActualEmployees = employeeRepository.getAllEmployees().size();
        assertEquals(totalExpectedEmployees, totalActualEmployees);
    }

    @Test
    void getEmployee_validRequest_returnsTrue() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int testId = 1;
        boolean expextedResult = true;
        boolean resultedResult = true;
        Employee employee = employeeRepository.getEmployee(testId);
        assertEquals(expextedResult, resultedResult);
    }

    @Test
    void getEmployee_nonexistentID_returnsFalse() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int testId = 4;
        boolean expectedResult = false;
        boolean resultedResult = false;
        Employee employee = employeeRepository.getEmployee(testId);
        assertEquals(expectedResult, resultedResult);
    }

    @Test
    void createEmployee_correctRequestBody_returnCreatedEmployee() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        Employee newEmployee = new Employee(4, 9473677053L, "James", "Bond", "2007-07-07", "Part-Timer", "operative", "2020-02-20", List.of("Walther PPK", "Aston Martin"));


        Employee expectedEmployee = newEmployee;

        Employee resultedEmployee = repository.createEmployee(newEmployee.getId(), newEmployee);

        assertEquals(expectedEmployee, resultedEmployee);
    }

    @Test
    void replaceEmployee_idAndEmployeeValid_returnReplacedEmployee() {

        EmployeeRepository employeeRepository = new EmployeeRepository();
        int testId = 1;
        Employee newEmployee = new Employee(4, 9473677053L, "James", "Bond", "2007-07-07", "Part-Timer", "operative", "2020-02-20", List.of("Walther PPK", "Aston Martin"));
        Employee expectedReplacedEmployee = newEmployee;
        Employee actualReplacedEmployee = repository.replaceEmployee(testId, newEmployee);

        assertEquals(expectedReplacedEmployee, actualReplacedEmployee);
    }

    @Test
    void deleteEmployee_deleteEmployeeForValidId_returnEmployeeToDelete() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int testId = 1;
        Employee expectedEmployee = new Employee(4, 9473677053L, "James", "Bond", "2007-07-07", "Part-Timer", "operative", "2020-02-20", List.of("Walther PPK", "Aston Martin"));

        Employee actualEmployee = employeeRepository.getEmployee(testId);
        assertEquals(expectedEmployee, actualEmployee);

    }

    @Test
    void checkIfEmployeeExistsById_validId_returnTrue() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int testId = 1;
        Boolean expectedResult = true;
        Boolean resultedResult = repository.employeeExistsById(testId);
        assertEquals(expectedResult, resultedResult);
    }




    }








